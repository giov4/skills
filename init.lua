skills = {}

dofile(minetest.get_modpath("skills") .. "/src/settings_loader.lua")

dofile(minetest.get_modpath("skills") .. "/src/utils.lua")
dofile(minetest.get_modpath("skills") .. "/src/chatcmdbuilder.lua")

dofile(minetest.get_modpath("skills") .. "/src/database.lua")

dofile(minetest.get_modpath("skills") .. "/src/physics_manipulation.lua")
dofile(minetest.get_modpath("skills") .. "/src/audio.lua")

dofile(minetest.get_modpath("skills") .. "/src/cooldown.lua")

dofile(minetest.get_modpath("skills") .. "/src/cast.lua")
dofile(minetest.get_modpath("skills") .. "/src/start.lua")
dofile(minetest.get_modpath("skills") .. "/src/stop.lua")

dofile(minetest.get_modpath("skills") .. "/src/attached_entities.lua")

dofile(minetest.get_modpath("skills") .. "/src/core.lua")

dofile(minetest.get_modpath("skills") .. "/src/commands.lua")

--dofile(minetest.get_modpath("skills") .. "/src/debug.lua")


