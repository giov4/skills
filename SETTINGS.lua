--!
--! DON'T TOUCH THIS.
--!																										
--! if you want to modify any of these settings just edit the file in <YOUR_WORLD_FOLDER>/skills/

skills.settings.chat_warnings = {
	cooldown = false,
	disabled = false
}